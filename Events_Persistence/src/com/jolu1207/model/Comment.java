package com.jolu1207.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Comment
 *
 */
@Entity
@Table(name = "comments")
@XmlRootElement
@NamedQuery(name="Comment.findAll", query="SELECT c FROM Comment c")
public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
	@SequenceGenerator(name="COMMENTS_ID_GENERATOR", sequenceName="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COMMENTS_ID_GENERATOR")
	private Long id;
    @Column(name = "COMMENT")
    private String comment;
    @Column(name = "LASTUPDATE")
    private LocalDateTime lastupdate;
    @Column(name = "TIME")
    private LocalDateTime time;
    @JoinColumn(name = "EVENT", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Event event;
    @JoinColumn(name = "USER1", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private User user1;

    public Comment() {
    }

    public Comment(Long id) {
        this.id = id;
    }

    public Comment(User user, LocalDateTime time, String comment) {
		user1=user;
		this.time=time;
		this.comment=comment;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(LocalDateTime lastupdate) {
        this.lastupdate = lastupdate;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @PrePersist
    @PreUpdate
    void updatedAt() {
      this.lastupdate = LocalDateTime.now();
    }
    
    @Override
    public String toString() {
        return "Comments[ id=" + id + " ]";
    }
    
}

