package com.jolu1207.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@Table(name = "events")
@XmlRootElement
@NamedQueries({
	@NamedQuery(name="Event.findAll", query="SELECT e FROM Event e"),
    @NamedQuery(name="Event.findByCity", query="SELECT e FROM Event e WHERE e.city = :city"),
}) 

public class Event implements Serializable {
    private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EVENTS_ID_GENERATOR", sequenceName="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EVENTS_ID_GENERATOR")
    private Long id;
	@Column(name= "GOOGLEID")
	private String googleID;
    public String getGoogleID() {
		return googleID;
	}
	public void setGoogleID(String googleID) {
		this.googleID = googleID;
	}
	@Column(name = "CITY")
    private String city;
    @Column(name = "CONTENT")
    private String content;
    @Column(name = "END1")
    private LocalDateTime endTime;
    @Column(name = "LASTUPDATE")
    private LocalDateTime lastupdate;
    @Column(name = "START")
    private LocalDateTime startTime;
    @Column(name = "TITLE")
    private String title;
    @ManyToMany(fetch= FetchType.EAGER)
     @JoinTable(name = "ORGANIZERS", joinColumns = {
        @JoinColumn(name = "EVENT", referencedColumnName = "ID",nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "USER1", referencedColumnName = "ID", nullable=false)})
    private List<User> usersList = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event", fetch= FetchType.EAGER)
    private List<Comment> commentsList=new ArrayList<Comment>();

    public Event() {
		// TODO Auto-generated constructor stub
	}
    public Event(String title, String city, String content, LocalDateTime startTime, LocalDateTime endTime, List<User> users, List<Comment> comments) {
    	this.title=title;
    	this.city=city;
    	this.content=content;
    	this.startTime=startTime;
    	this.endTime=endTime;
    	this.usersList=users;
    	this.commentsList=comments;
    }

    public Event(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime  getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public LocalDateTime getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(LocalDateTime lastupdate) {
        this.lastupdate = lastupdate;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStart(LocalDateTime start) {
        this.startTime = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @XmlTransient
    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    @XmlTransient
    public List<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comment> commentsList) {
        this.commentsList = commentsList;
    }
	public Comment addComment(Comment comment) {
		getCommentsList().add(comment);
		comment.setEvent(this);
		return comment;
	}

	public Comment removeComment(Comment comment) {
		getCommentsList().remove(comment);
		comment.setEvent(null);

		return comment;
        }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Event)) {
            return false;
        }
        Event other = (Event) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    @PrePersist
    @PreUpdate
    void updatedAt() {
      this.lastupdate = LocalDateTime.now();
    }
    @Override
    public String toString() {
        return "Events[ id=" + id + " ]";
    }
    
}
