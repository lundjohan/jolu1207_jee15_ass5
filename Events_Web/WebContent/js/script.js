/**
 * 
 */

$(function() {
	
	
	/*
	 * Enlarge block of info for one event when that block is clicked
	 */
	$("ul").click(function() {	 
		var $list_elem = $(this).children().not(".title");
		if($list_elem.hasClass("invisible")){
			$list_elem.removeClass("invisible");
		}
		else{
			$list_elem.addClass("invisible");
		}
	
	});
	
	/*
	 * function acts with id=filterSearch to
	 * display only those events which city corresponds to its value.
	 * Function inspired by http://stackoverflow.com/questions/9127498/how-to-perform-a-real-time-search-and-filter-on-a-html-table
	 * 
	 */
	
	var $event = $("ul"); 
	$('#filterSearch').keyup(function() {
		var val = $.trim($(this).val()).toLowerCase();

		$event.hide().filter(function() {
			var cityStr = $(this).children('.city').text().toLowerCase();
			return cityStr.slice(0, val.length) == val; //if true=> 
		}).show();						//...show row
	});
});