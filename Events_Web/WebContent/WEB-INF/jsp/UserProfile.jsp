<%@page isELIgnored="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>

<html>
<head>
<title>User Profile</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/script.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="content">
			<h3>Data for ${user.firstname} ${user.lastname}</h3>
			<img src="${user.image}" alt="image" style="height: 5em;">
			<p>Email: ${user.id}</p>
			<p>${nrOfPastEvents} organized events in the past</p>
			<p>${nrOfFutureEvents} upcoming events as organizer</p>
			<p>${nrOfComments} comments made</p>
			<c:url var="url" value="/EventsOverview" />
			<p>
				Back to <a href="${url}">EventsOverview</a>
			</p>
		</div>
		<footer>
			<p>Page Views ${counter}
			<p>
		</footer>
	</div>
</body>
</html>