<%@page isELIgnored="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>

<html>
<head>
<title>Event Added</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/script.js"></script>
</head>
<body>
<c:choose>
  <c:when test="${eventAddedSuccess}">
  <p>Event has been added!</p>
  </c:when>
  <c:otherwise>
    <p>Sorry!Something went wrong and event couldn't be added.</p>
  </c:otherwise>
</c:choose>
	<c:url var="url" value="EventsOverview" />
	<p>Return to <a href="${url}">EventsOverview</a></p>
<footer><p></p>Page Views ${counter}<p></footer>
</body>
</html>