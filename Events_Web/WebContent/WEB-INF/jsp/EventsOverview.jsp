<%@page isELIgnored="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>

<html>
<head>
<title>Events Overview</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/script.js"></script>
</head>
<body>
<c:url var="url" value="/NewEvent" />
<div id = "wrapper">
<div id = "content">
<h3>Upcoming Events</h3>
<div><input type="button" value="add event" onclick="document.location.href='${url}'">
	<input type="text" id="filterSearch" placeholder="Filter by location"></div>
		<c:forEach var="event" items="${futureEvents}">

			<ul>
				<li class ="title">${event.title}</li>
				<li class="city invisible">${event.city}</li>
				
				<li class = "invisible">Start Time: ${event.startTime}</li>
				<li class = "invisible">End Time: ${event.endTime}</li>
				
				
				<c:forEach var="comment" items="${event.commentsList}">
				<li class = "invisible">Comment: ${comment.comment}</li>
				</c:forEach>
				<c:forEach var="user" items="${event.usersList}">
				
				<c:url var="url" value="UserProfile">  
                  <c:param name="userID" value="${user.id}"/>    
                </c:url>
               
				<li class = invisible>Organizer: <a href="${url}">${user.firstname}
					${user.lastname}</a></li>
				</c:forEach>
			</ul>
			
		</c:forEach>
		
		</div>
		
<footer><p>Page Views ${counter}<p></footer>
</div>
</body>
</html>