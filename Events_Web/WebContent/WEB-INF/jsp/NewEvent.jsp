<%@page isELIgnored="false" language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>

<html>
<head>
<title>New Event</title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/script.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="content">
			<h3>Fill in form for a new Event</h3>
			<c:url var="url" value="EventAdded" />
			<form action="${url}" method="post">
				<label for="title">Title of Event:</label><br> <input
					type="text" name="title" value="Dogsled Ride"> <br> <label
					for="city">City of Event:</label><br> <input type="text"
					name="city" value="Piteå"> <br> <label for="content">Content
					of Event:</label><br> <input type="text" name="content"
					value="Go on a dogsled ride through the forest."> <br>
				<label for="start">Start of Event:</label><br> <input
					type="text" name="start" value="16/01/29 10:00"> <br>
				<label for="end">End of Event:</label><br> <input type="text"
					name="end" value="16/01/29 18:00"> <br> <label
					for="city">Organizer of Event:</label><br> <select
					name="organizer">
					<c:forEach var="user" items="${userList}">
						<option value="${user.id}">${user.firstname}
							${user.lastname}</option>
					</c:forEach>
				</select> <br> <input type="submit" value="Submit" />
			</form>
			<br>
			<c:url var="url" value="EventsOverview" />
			<p>
				Back to <a href="${url}">EventsOverview</a>
			</p>
		</div>
		<footer>
			<p>Page Views ${counter}
			<p>
		</footer>
	</div>
</body>
</html>
