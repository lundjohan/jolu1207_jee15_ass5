package com.jolu1207.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jolu1207.counter.PageCounters;
import com.jolu1207.counter.PageCountersBean;
import com.jolu1207.format.DateFormatter;
import com.jolu1207.logic.BusinessLayer;
import com.jolu1207.model.Comment;
import com.jolu1207.model.Event;
import com.jolu1207.model.User;

/**
 * Servlet implementation class DispatcherServlet
 */
@WebServlet(name = "Dispatcher", urlPatterns = { "/EventsOverview", "/NewEvent", "/UserProfile", "/EventAdded" })
public class DispatcherServlet extends HttpServlet {
	@EJB
	BusinessLayer business;

	@Inject
	PageCounters pcBean;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String selectedScreen = request.getServletPath();
		System.out.println("selectedScreen: " + selectedScreen);

		// for pageCounter => retrieve an unique name for every page.
		String pageName = selectedScreen + request.getQueryString();
		request.setAttribute("counter", pcBean.getCount(pageName));

		if (selectedScreen.equals("/EventsOverview")) {
			request.setAttribute("futureEvents", business.getFutureEvents());
		}

		else if (selectedScreen.equals("/NewEvent")) {
			request.setAttribute("userList", business.getUsers());
		}

		else if (selectedScreen.equals("/UserProfile")) {
			String userID = request.getParameter("userID");
			System.out.println("userID inside dispatcher: " + userID);
			request.setAttribute("user", business.getUser(userID));

			long nrFuture = business.getNrOfUsersFutureEvents(userID.trim());
			request.setAttribute("nrOfFutureEvents", nrFuture);

			long nrPast = business.getNrOfUsersPastEvents(userID.trim());
			request.setAttribute("nrOfPastEvents", nrPast);

			long nrComments = business.getNrOfComments(userID.trim());
			request.setAttribute("nrOfComments", nrComments);

		} else if (selectedScreen.equals("/EventAdded")) {
			String title = request.getParameter("title");
			String city = request.getParameter("city");
			String content = request.getParameter("content");
			LocalDateTime startTime = DateFormatter.format(request.getParameter("start").trim());
			LocalDateTime endTime = DateFormatter.format(request.getParameter("end").trim());
			String userId = request.getParameter("organizer");

			User user = business.getUser(userId);

			List<User> userList = new ArrayList<User>();
			userList.add(user);

			// create empty List comments
			List<Comment> comments = new ArrayList<Comment>();

			Event event = new Event(title, city, content, startTime, endTime, userList, comments);
			if (business.addEvent(event, false)) // if success
				request.setAttribute("eventAddedSuccess", true);
			else
				request.setAttribute("eventAddedSuccess", false);
		}

		String screen = "/WEB-INF/jsp/" + selectedScreen + ".jsp";

		try {
			request.getRequestDispatcher(screen).forward(request, response);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
