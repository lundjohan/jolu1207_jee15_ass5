package com.jolu1207.counter;

import java.util.HashMap;

import javax.ejb.Singleton;
import javax.enterprise.context.ApplicationScoped;

/**
 * Session Bean implementation class PageCountersBean
 */
@ApplicationScoped
public class PageCountersBean implements PageCounters{

	private HashMap<String, PageCounter> pcMap;

	public PageCountersBean() {
		pcMap = new HashMap<>();
	}

	@Override
	public int getCount(String pageName) {
		PageCounter pc;
		if (!pcMap.containsKey(pageName))
			pc = new PageCounter();
		else
			pc = pcMap.get(pageName);
		pc.increment();
		pcMap.put(pageName, pc);
		return pc.getCounter();
	}
}
