package com.jolu1207.counter;

import java.util.concurrent.atomic.AtomicInteger;

public class PageCounter{
    private AtomicInteger counter = new AtomicInteger(0);
 
    public PageCounter(){}
    public void increment() {
        counter.incrementAndGet();
    }
    public int getCounter() {
        return counter.get();
    }
 
}

