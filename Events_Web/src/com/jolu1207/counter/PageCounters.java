package com.jolu1207.counter;

import javax.ejb.Remote;

@Remote
public interface PageCounters {

	public int getCount(String pageName);
}