package com.jolu1207.logic;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;

import com.jolu1207.calendar.CalendarInt;
import com.jolu1207.calendar.City;
import com.jolu1207.model.Event;
import com.jolu1207.model.User;

@Singleton
public class BusinessLayerBean implements BusinessLayer {

	private static final Lock eventsLock = new ReentrantLock();

	@Resource
	private SessionContext context;

	@EJB
	private FillDataBase fillDB;
	@EJB
	private ManagerBean manager;

	@EJB
	private CalendarInt calendar;

	@PostConstruct
	public void init() {
		String url = "https://dl.dropboxusercontent.com/u/35363454/javaee/lo1/material/events.txt";
		fillDB.run(url);
		try {
			calendar.deleteAllCals(fillDB.getEvents());
		} catch (IOException e) {
			e.printStackTrace();
		}

		manager.persistAll(fillDB.getEntities()); 
		addEvents(fillDB.getEvents());

	}

	public void addEvents(List<Event> events) {

		for (Event event : events) {
			addEvent(event, true);
		}
	}

	public boolean addEvent(Event event, boolean updateDB) {

		//booleans/ conditions used to which Google Calendar actions to rollback.
		boolean newLocationCreated = false,	 //If new secondary calendar (with name of events city) added in Google Calendar
				eventAdded = false;			//if event has been added to google calendar
		
		City city = null;

		try {

			eventsLock.tryLock();

			city = calendar.getCity(event.getCity()); 

		
			if (city == null) { 
				city = calendar.createCalendar(event.getCity());
				newLocationCreated = true;
			}

			System.out.println(
					"Inuti BusinessLayerBean => CalendarID" + city.getCalendarId() + " name:" + city.getCityName()); 
																														
			String googleID = calendar.addEvent(city, event);
			eventAdded = true; // komplettering. Tillagd.

			event.setGoogleID(googleID);

			if (!updateDB) {
				manager.addEvent(event);
			}

		} catch (Exception e) { // catch flera olika typer av exception, denna
								// om

			System.out.println(e.getMessage());
			context.setRollbackOnly();

			// NOTE: use retry on delete statements here upon failure?

			if (newLocationCreated && city != null) {

				calendar.deleteCity(city);

			} else if (eventAdded) { // komplettering => tidigare enbart else.

				calendar.deleteEvent(city, event); // komplettering => denna
													// kraschar!
			}

		} finally {

			eventsLock.unlock();

		}
		if (!eventAdded)
			return false;
		return true;
	}

	public List<Event> getFutureEvents() {
		return manager.getFutureEvents();
	}

	public long getNrOfUsersFutureEvents(String userID) {
		return manager.getNrOfUsersFutureEvents(userID);
	}

	public long getNrOfUsersPastEvents(String userID) {
		return manager.getNrOfUsersPastEvents(userID);
	}

	public long getNrOfComments(String userID) {
		return manager.getNrOfComments(userID);
	}

	public User getUser(String userID) {
		return manager.getUser(userID);
	}

	public List<User> getUsers() {
		return manager.getUsers();
	}

}
