package com.jolu1207.logic;

import java.util.List;

import javax.ejb.Remote;

import com.jolu1207.model.Event;
import com.jolu1207.model.User;

@Remote
public interface BusinessLayer {

	public boolean addEvent(Event event, boolean updateDB);
	public List<Event> getFutureEvents();
	public long getNrOfUsersFutureEvents(String userID);
	public long getNrOfUsersPastEvents(String userID);
	public long getNrOfComments(String userID);
	public User getUser(String userID);
	public List<User> getUsers();
}
