package com.jolu1207.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;

import com.jolu1207.format.DateFormatter;
import com.jolu1207.model.Comment;
import com.jolu1207.model.Event;
import com.jolu1207.model.User;

/**
 * Session Bean implementation class FillDataBase
 */
@Singleton
public class FillDataBaseBean implements FillDataBase {
	private String fileLoc;
	private List<User> users;
	private List<Event> events;
	private List<Comment> allComments;
	
	public FillDataBaseBean(){
		users = new ArrayList<User>();
		events = new ArrayList<Event>();
		allComments = new ArrayList<Comment>();
	}
	@Override
	public void run(String fileLoc){
		this.fileLoc = fileLoc;
		read();
		
	}
	@Override
	public List<Event>getEvents(){
		
		return events;
		
	}
	@Override
	public List<User> getUsers() {
		return users;
	}
	
	@Override
	public List<Comment> getComments() {
		return allComments;
	}
	
	@Override
	public List<Object> getEntities(){	
		
		List<Object> objects = new ArrayList<>();
		objects.addAll(allComments);
		objects.addAll(users);
		objects.addAll(events);
		return objects;
	}
	private void read() {
		URL url;
		try {
			url = new URL(fileLoc);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.contains("Users")) {
					readUsers(reader);
				} else if (line.contains("Events")) {
					readEvents(reader);
				}

			}
		} catch (IOException x) {
			x.printStackTrace();
			System.err.format("IOException: %s%n", x);
		}
	}

	
	private void readUsers(BufferedReader reader) throws IOException {
		String line = null;
		while ((line = reader.readLine()) != null) {
			if (line.isEmpty())
				return;
			String[] values = line.split("\t");
			String[] name = values[0].split("\\s+");
			User u = new User(values[1], name[0], name[1]);
			
			//add image. Not optimal code, img-url will fail if folders are moved in project
			u.setImage("img/"+values[1]+".jpg");
			
			users.add(u);

		}
	}

	private void readEvents(BufferedReader reader) throws IOException {

		String line = null;
		List<Comment> comments = null;
		String title = null;
		String city = null;
		String content = null;
		LocalDateTime startTime = null;
		LocalDateTime endTime = null;
		List<User> userList = null;
		while ((line = reader.readLine()) != null) {
			comments = new ArrayList<Comment>();
			String[] values = line.split(",");
			title = values[0].trim();
			city = values[1].trim();
			content = values[2].trim();
			String[] dates = values[3].split("-");
			startTime = DateFormatter.format(dates[0].trim());
			endTime = DateFormatter.format(dates[1].trim());
			userList = getUserList(line.substring(line.indexOf('[')));
			comments = getCommentForEvent(reader);
			allComments.addAll(comments);
			Event e = new Event(title, city, content, startTime, endTime, userList, comments);
			for (Comment comment : comments) {
				comment.setEvent(e);

			}
			events.add(e);
		}
	}

	private List<Comment> getCommentForEvent(BufferedReader reader) throws IOException {
		String line = null;
		List<Comment> comments = new ArrayList<Comment>();
		while ((line = reader.readLine()) != null && !line.isEmpty()) {

			User user = findUserById(line.substring(0, line.indexOf('(')).trim());
			
			LocalDateTime time = DateFormatter.format(line.substring(line.indexOf('(') + 1, line.indexOf(')')).trim());
			String commentStr = line.substring(line.indexOf(": \"") + 3, line.length() - 1);
			comments.add(new Comment(user, time, commentStr));
		}
		return comments;

	}

	private List<User> getUserList(String usersStr) {
		usersStr = usersStr.substring(1, usersStr.length() - 1);
		String[] usersArr = usersStr.split(",\\s+");
		List<User> result = new ArrayList<User>();
		for (String u : usersArr) {
			User user = findUserById(u);
			if (user != null)
				result.add(user);
		}
		return result;
	}

	private User findUserById(String id) {
		for (User user : users) {
			if (id.equalsIgnoreCase(user.getId()))
				return user;
		}
		return null;
	}
}
