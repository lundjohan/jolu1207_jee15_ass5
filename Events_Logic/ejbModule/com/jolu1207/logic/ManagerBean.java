package com.jolu1207.logic;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.jolu1207.model.Comment;
import com.jolu1207.model.Event;
import com.jolu1207.model.User;

/**
 * Session Bean implementation class emBean
 */
@Stateless
public class ManagerBean {
	@PersistenceContext(unitName = "events")
	EntityManager em;

	//testing
	public void persistEvents(List<Event> events) {
		for (Event evt : events)
			em.persist(evt);
	}
	public void persistUsers(List<User> users) {
		for (User usr : users)
			em.persist(usr);
	}
	public void persistComments(List<Comment> comments) {
		for (Comment cmt : comments)
			em.persist(cmt);
	}
	
	//slut test
	
	
	public void persistAll(List<Object> objects) {
		for (Object obj : objects)
			em.persist(obj);
	}

	public void addEvent(Event event) {
		em.persist(event);
	}

	private Query getQuery(String queryStr) {
		return em.createQuery(queryStr);
	}

	public List<Event> getFutureEvents() {
		List<Event> res = (List<Event>) getQuery("select event from Event event where event.startTime>:now")
				.setParameter("now", LocalDateTime.now()).getResultList();
		return res;
	}
	
	public List<Comment> getComments(String eventId) {
		List<Comment> res = (List<Comment>) getQuery("select comment from Comment comment");
				//.setParameter("now", LocalDateTime.now()).getResultList();
		return res;
	}

	public long getNrOfUsersFutureEvents(String userID) {
		return (Long) getQuery(
				"SELECT COUNT(u) FROM User u INNER JOIN u.eventsList e WHERE u.id LIKE :user_id AND e.startTime>:now")
						.setParameter("user_id", userID).setParameter("now", LocalDateTime.now()).getSingleResult();
	}

	public long getNrOfUsersPastEvents(String userID) {
		return (Long) getQuery(
				"SELECT COUNT(u) FROM User u INNER JOIN u.eventsList e WHERE u.id LIKE :user_id AND e.startTime<:now")
						.setParameter("user_id", userID).setParameter("now", LocalDateTime.now()).getSingleResult();
	}

	public long getNrOfComments(String userID) {
		return (Long) getQuery("SELECT COUNT(c) FROM Comment c WHERE c.user1.id LIKE :user_id")
		//return (int) getQuery("SELECT SIZE(u.commentsList) FROM User u WHERE u.id LIKE :user_id")
				.setParameter("user_id", userID).getSingleResult();
	}

	public User getUser(String userID) {
		return (User) getQuery("SELECT u FROM User u where u.id = :user_ID").setParameter("user_ID", userID)
				.getSingleResult();
	}

	public List<User> getUsers() {
		List<User> res = (List<User>) getQuery("select distinct user from User user").getResultList();
		return res;
	}

}