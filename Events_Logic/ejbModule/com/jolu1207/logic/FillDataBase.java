package com.jolu1207.logic;

import java.util.List;

import javax.ejb.Remote;

import com.jolu1207.model.Comment;
import com.jolu1207.model.Event;
import com.jolu1207.model.User;

@Remote
public interface FillDataBase {
	public List<Object> getEntities();
	public List<Event> getEvents();
	public List<User> getUsers();
	public List<Comment> getComments();
	void run(String fileLoc);
}
