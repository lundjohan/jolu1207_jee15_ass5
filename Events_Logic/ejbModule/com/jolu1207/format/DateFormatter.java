package com.jolu1207.format;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import com.google.api.client.util.DateTime;

public class DateFormatter {
	public static DateTimeFormatter formatter 
	= DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
	public static LocalDateTime format(String str) {
		return LocalDateTime.parse(str,formatter);
	}
	public static DateTime convertLocalDateTime(LocalDateTime ldt){
		//1. convert to Date
		ZoneOffset offset = ZoneOffset.of("+01:00");
		return new DateTime(ldt.toEpochSecond(offset)*1000); //*1000 => seconds to ms
	}
}
