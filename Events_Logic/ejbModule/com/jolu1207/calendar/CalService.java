package com.jolu1207.calendar;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.jolu1207.format.DateFormatter;

/**
 * Assuming that you collected your refresh token via the ApplicationTest class,
 * you can now test if everything works. You could include parts of the code
 * below in order to get access to the calendars from within your enterprise
 * application.
 * 
 * @author Felix Dobslaw
 */
@ApplicationScoped
public class CalService {

	//jolu1207.jee15@gmail.com, Project Name: jolu1207.jee15.ass5, Client name: jolu1207_jee15_ass5  
	private final String clientID = "295458576388-dsnog8qb135909p957k70tbpt56331ha.apps.googleusercontent.com";
	private final String clientSecret = "-cYImMhRToeeK7Q_zSuhBYeA";
	private final String refreshToken = "1/d2tpP1ddL_Qz7L3o5eZbBH3J7knwSmdPi_y9cGQmiZ9IgOrJDtdun6zK6XiATCKT";
	

	/*
	 * T
	private final String clientID = "432741790158-i4h216mj4i0p2di20gum4a8eedfvh17h.apps.googleusercontent.com";
	private final String clientSecret = "wD0lWCPL3Qpy06xFRV9C3-Ra";
	private final String refreshToken = "1/X3D1zJ1dlKdyEFqskqvuTqOq8BDF5lB5Yqjb8Jg5I3BIgOrJDtdun6zK6XiATCKT";
*/
	// testingjavaee
	/*
	 * String clientID =
	 * "901766222596-na7hqruh6nih7gb9hj3mhj440m1042jf.apps.googleusercontent.com";
	 * String clientSecret = "N5M_W1c54olSYJJbDInY1WU0"; String refreshToken =
	 * "1/ebl6h2DD-qfeM-f70JpgvocwqbfOquuzxn41Megxwa8";
	 */
	//testingjavaee2
	/*private final String clientID = "492479013988-mt547prqtc3bu3latuu1uu6dq7cer7bn.apps.googleusercontent.com";
	private final String clientSecret = "6sA9lX9zTc_5Ht3s9Szwi1Rp";
	private final String refreshToken = "1/Dy_h8JDR2BJiFhvSmkXHewzRDb5cfLGo5gKHOK5r_9A";
*/
	private JacksonFactory jsonFactory;
	private Calendar service;

	private NetHttpTransport httpTransport;

	public CalService() {
	}

	@PostConstruct
	private void initService() {

		httpTransport = new NetHttpTransport();
		jsonFactory = new JacksonFactory();
		GoogleCredential credential = new GoogleCredential.Builder().setClientSecrets(clientID, clientSecret)
				.setJsonFactory(jsonFactory).setTransport(httpTransport).build().setRefreshToken(refreshToken);

		service = new Calendar.Builder(httpTransport, jsonFactory, credential).build();

	}

	// Delete a secondary calendar
	public void deleteCal(String city) throws IOException {
		CalendarList calendarList = service.calendarList().list().execute();

		for (CalendarListEntry calendarListEntry : calendarList.getItems()) {

			if (calendarListEntry.getSummary().equals(city)) {

				System.out.println("Deleting " + calendarListEntry.getId() + " " + calendarListEntry.getSummary());

				service.calendars().delete(calendarListEntry.getId()).execute();
			}

		}

		
	}

	// define a list of attendees to an event
	private EventAttendee[] addAttendees(List<String> emails) {
		EventAttendee[] attendees = new EventAttendee[emails.size()];
		for (int i = 0; i < emails.size(); i++) {
			attendees[i] = new EventAttendee().setEmail(emails.get(i));
			System.out.println("email: " + emails.get(i));
		}
		return attendees;
	}

	private String getCalendarID(String city) throws IOException {

		CalendarList calendarList = service.calendarList().list().execute();

		for (CalendarListEntry calendarListEntry : calendarList.getItems()) {
			if (calendarListEntry.getSummary().equals(city)) {
				System.out.println("The calendars are equal for: " + city);
				return calendarListEntry.getId();
			}
		}
		return null;
	}

	public City getCity(String city) throws IOException {
		String calId = null;
		calId = getCalendarID(city);
		
		if (calId != null) {
			return new City(calId, city);
		}
		
		return null;
	}

	public City createCalendar(String city) throws IOException {
		com.google.api.services.calendar.model.Calendar calendar = new com.google.api.services.calendar.model.Calendar();
		calendar.setSummary(city);
		calendar.setTimeZone("Europe/Stockholm");
		System.out.println("service.getBaseUrl(): " + service.getBaseUrl());
		String calendarID = service.calendars().insert(calendar).execute().getId();
		return new City(calendarID, city);

	}

	public void deleteEvent(City city, String eventId) throws IOException {

		service.events().delete(city.getCalendarId(), eventId).execute();

	}

	public void deleteCity(City city) throws IOException {
		service.calendars().delete(city.getCalendarId()).execute();
	}

	public String insertEvent(City city, String summary, String description, LocalDateTime startTime,
			LocalDateTime endTime, List<String> emails) throws IOException {
		System.out.println("Adding callendar: " + city);

		// Create the Event
		// summary, city==location, description
		Event event = new Event().setSummary(summary).setLocation(city.getCityName()).setDescription(description);

		// start & end
		DateTime startDateTime = DateFormatter.convertLocalDateTime(startTime);
		System.out.println("DateFormatter.convertLocalDateTime(startTime).toString()"
				+ DateFormatter.convertLocalDateTime(startTime).toString());
		EventDateTime start = new EventDateTime().setDateTime(startDateTime).setTimeZone("Europe/Stockholm");
		event.setStart(start);

		DateTime endDateTime = DateFormatter.convertLocalDateTime(endTime);
		EventDateTime end = new EventDateTime().setDateTime(endDateTime).setTimeZone("Europe/Stockholm");
		event.setEnd(end);

		EventAttendee[] attendees = addAttendees(emails);
		event.setAttendees(Arrays.asList(attendees));

		// System.out.println("emails.size() " + emails.size());
		event = service.events().insert(city.getCalendarId(), event).execute();

		// bekräftelse till prompt
		System.out.printf("Event created: %s, id: %s\n", event.getHtmlLink(), event.getId());
		return event.getId();
	}
}
