package com.jolu1207.calendar;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;

import com.jolu1207.model.Event;
import com.jolu1207.model.User;

/**
 * Session Bean implementation class CalendarBean
 */
@Singleton(mappedName = "CalendarBean")
public class CalendarBean implements CalendarInt {
	@Inject
	private CalService calService;

	public CalendarBean() {
	}
	public City getCity(String city) throws IOException{
		return calService.getCity(city);
		
	}
	@Override
	public void deleteAllCals(List<Event>events) throws IOException{
		for(Event event: events){
			calService.deleteCal(event.getCity());
		}
	}
	
	@Override
	public City createCalendar(String city) throws IOException {
		return calService.createCalendar(city);
		
	}
	
	@Override
	public String addEvent(City city, Event event) throws IOException{
		String summary = event.getTitle();
		String description = event.getContent();
		LocalDateTime start = event.getStartTime();
		LocalDateTime end = event.getEndTime();
		List<User> organizers = event.getUsersList();
		List<String> attendees = new ArrayList<>();
		for (User usr : organizers)
			attendees.add(usr.getId());
		return calService.insertEvent(city, summary, description, start, end, attendees);
	}
	
	@Override
	public boolean deleteEvent(City city, Event event) {

		try {
			
			calService.deleteEvent(city, event.getGoogleID());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return false;
		
	}
	
	@Override
	public boolean deleteCity(City city) {

		try {
			
			calService.deleteCity(city);
			
			return true;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return false;
		
	}

	

}
