package com.jolu1207.calendar;

import com.jolu1207.model.Event;

public class City {
	private String calendarId;
	private String cityName;

	public City(String calendarId, String cityName) {
		this.calendarId = calendarId;
		this.cityName = cityName;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public String getCityName() {
		return cityName;
	}

}
