package com.jolu1207.calendar;

import java.io.IOException;
import java.util.List;

import javax.ejb.Remote;

import com.jolu1207.model.Event;

@Remote
public interface CalendarInt {
	
	
	
	
	public boolean deleteCity(City city);
	/**
	 * Removes an event from the event list. Returns true if an event was removed.
	 * @param event
	 * @return
	 */
	public boolean deleteEvent(City city, Event event);
	public String addEvent(City city, Event event) throws IOException;
	public City getCity(String city) throws IOException;
	public City createCalendar(String city) throws IOException;
	void deleteAllCals(List<Event> events) throws IOException;

}
