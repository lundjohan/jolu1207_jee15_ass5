# Assignment 5. Johan Lund. #

## Completion/ Komplettering ##
2016-01-28

### Problem: ###
Completion was about that server crasched if events was being added when connection had been lost to Google Calendar. 

### Solution: ###

I added the boolean eventAdded inside BusinessLayerBean.addEvent. If connection is lost before event is added the program no longer tries to delete that event in Google Calendar during rollback.
  I also changed in DispatcherServlet and EventAdded.jsp so that now it is printed to web if event adding was success or failure. I changed BusinessLayerBean.addEvent's return type to a boolean for that reason.

## Laborationsdata ##
Namn: 		Johan Lund.
Loginid i moodle: 	jolu1207
Laborationens namn:	Assignment 5: Transactional Management
Datum:		        2016-01-17 
Utvecklingsmiljö:	Eclipse Mars( jdk 1.8, JPA 2.1, hibernate, postgreSQL + Pgadmin, Windows 7  (för Windows), Glassfish 4.1, Google Calendar.
Referens:	java ee 7 tutorial (pdf) with examples mostly on EJB's,  Fowler - Patterns of Enterprise Application Architecture, StackOverflow (used in overflow).

##Files sent in Moodle ##
Only this md-file.

## Description of how the app works##

Application starts (app is loaded on glassfish) when user opens 
http://localhost:8080/Events_Web/
.
The controller sets in (DispatcherServlet) and in it is BusinessLayerBean. BusinessLayerBean is the "Spider" in the project, in its <init()> function, the database is filling from events.txt (via class FillDataBase).
The very first thing BusinessLayerBean does is to delete all Calendars in Google with same cityname(Summary) as the events that comes from events.txt/FillDataBaseBean.
The only Calendars left are ones that have other names (I dont delete them because some of them seem not to be allowed to be deleted, like Födelsedagar etc).
Then I persist objects through database and also puts them in Google Calendar. **The Function BusinessLayerBean.addEvent is the essential one here**. It is also the one used when event is added from my GUI. It is atomic, it uses SessionContext and setRollBackOnly if something goes wrong. I have put a boolean newLocationCreated (read it as "has new Secondary Calendar been created with this event where sometging went wrong?") and I use it to know if delete google secondary calendar in rollback or no. It has a lock on ( I am using ReentrantLock()).

Inloading of data from events.txt is not perfect. I consider these "errors" of minor importance since it is unrealistic from a REAL application to load in the events like this. These "errors" would not remain when this "testing" is stopped being implemented.
 
**I have set another attribute in Event.java=> GoogleId (it mirrors a google event id).** It has no function in my program but I heard from a computer guy friend that it is good to mirror in such ways.
  It is here the "error" from reading in the events.txt file set in: google-id is not set in these Events. It is only set in events that are added manually from GUI. The other (and last)"error" is that if something goes wrong with filling the database + filling google Calendars ( I am here using the boolean updateDB in BusinessLayerBean.addEvent) in the beginning it is not rolled back perfectly, I am though thrpwing an exception. However as I have argued above I consider this of minor importance since this "loading in of events" will not remain if application were serious. These both things work perfectly when events are added manually. 

BusinessLayerBean uses CalendarBean which uses CalService. CalService takes care of starting communication and getting and sending request to Google Calendar

## Problems I had ##
Understanding of persistence. First I had BusinessLayerBean as @ApplicationScoped and then I could inject it in ServletDispatcher.java without problem. But I wanted it as a Singleton, then glassfish made this complain:

Excpetion - Exception Description: An attempt was made to traverse a relationship using indirection that had a null Session. This often occurs when an entity with an uninstantiated LAZY relationship is serialized and that lazy relationship is traversed after serialization. To avoid this issue, instantiate the LAZY relationship prior to serialization.

I then made a less-than-optimal change: I made relationships in entities as EAGER. I simply couldnt find other solution of the problem.

Another thing: It would have been easier if I could have persisted events, users and comments at different times. It took sometimes before I realized they have to be at least persisted in same transacstion. Otherwise this error (or coplaints about other relationsships amongst the entities):
java.lang.IllegalStateException: During synchronization a new object was found through a relationship that was not marked cascade PERSIST: Users[ id=per.ekeroot@miun.se ]. 

Another problem is ( I guess wellknown for teacher correcting):
{
  "code" : 403,
  "errors" : [ {
    "domain" : "usageLimits",
    "message" : "Calendar usage limits exceeded.",
    "reason" : "quotaExceeded"
  } ],
  "message" : "Calendar usage limits exceeded."
  
  It is the same for the Calendar I am using right now(jolu1207.jee15@gmail.com), I solved through creating multitudes of accounts.
  

